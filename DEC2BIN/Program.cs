﻿using System;
namespace DEC2BIN
{
    class Program
    {
        static void Main()
        {
            int D, X = 0;
            double B = 0;
            Console.Write("\n Bil.DEC = ");
            D = int.Parse(Console.ReadLine());
            while(D>0)
            {
                B = B + D % 2 * Math.Pow(10, X);
                D = D / 2;
                X++;
            }
            Console.Write("\n Bil.BIN = " + B);
            Console.ReadKey();
        }
    }
}
